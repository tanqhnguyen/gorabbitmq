package gorabbitmq

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/streadway/amqp"
)

// Runner interface describes an "long running" queue, it's typically queue that waits for messages
type Runner interface {
	RunWithExitChannel(exit chan os.Signal)
	Run()
}

// ConsumerFunction is the type for the consumer function
type ConsumerFunction = func(payload amqp.Delivery)

// NewReadableQueue creates a new Queue instance
func NewReadableQueue(channel *amqp.Channel) ReadableQueue {
	return ReadableQueue{
		Channel:     channel,
		ConsumerIDs: []string{},
		ConsumerFns: []func(*sync.WaitGroup){},
	}
}

// ReadableQueue is a wrapper around a channel to handle various common read operations
type ReadableQueue struct {
	Channel     *amqp.Channel
	ConsumerIDs []string
	ConsumerFns []func(*sync.WaitGroup)
}

// RegisterConsumer registers a new consumer for a queue. This consumer doesn't immediately listen to the message queue
func (mq *ReadableQueue) RegisterConsumer(queueName string, autoAck bool, prefetchCount int, consume ConsumerFunction) {
	consumer := GenerateNewConsumerID(queueName)

	mq.Channel.Qos(prefetchCount, 0, false)
	messages, err := mq.Channel.Consume(
		queueName, // queue
		consumer,  // consumer
		autoAck,   // auto-ack
		false,     // exclusive
		false,     // no-local
		false,     // no-wait
		nil,       // args
	)
	if err != nil {
		panic(err)
	}

	mq.ConsumerIDs = append(mq.ConsumerIDs, consumer)

	mq.ConsumerFns = append(mq.ConsumerFns, func(wg *sync.WaitGroup) {
		for delivery := range messages {
			consume(delivery)
		}
		wg.Done()
	})
}

// RunWithExitChannel executes all the registered consumers and let the caller
// decide when to exit
func (mq *ReadableQueue) RunWithExitChannel(exit chan os.Signal) {
	wg := new(sync.WaitGroup)

	for _, consume := range mq.ConsumerFns {
		wg.Add(1)
		go consume(wg)
	}

	<-exit

	for _, consumerID := range mq.ConsumerIDs {
		mq.Channel.Cancel(consumerID, false)
	}

	wg.Wait()
	fmt.Println("Exiting....")
}

// Run executes all the registered consumers and exits as soon as it receives
// either one of os.Interrupt, syscall.SIGINT, syscall.SIGTERM signal
func (mq *ReadableQueue) Run() {
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	defer mq.Channel.Close()

	mq.RunWithExitChannel(exit)
}

// NewWritableQueue returns a new WritableQueue instance. It can be used in normal or confirm mode
// by flipping `confirmMode` flag
func NewWritableQueue(channel *amqp.Channel, confirmMode bool) WritableQueue {
	if confirmMode {
		err := channel.Confirm(false)
		if err != nil {
			panic(err)
		}
	}

	return WritableQueue{
		Channel:      channel,
		ConfirmMode:  confirmMode,
		ConfirmsChan: channel.NotifyPublish(make(chan amqp.Confirmation, 1)),
	}
}

// WritableQueue is a wrapper around a channel to handle various common write operations
type WritableQueue struct {
	Channel      *amqp.Channel
	ConfirmMode  bool
	ConfirmsChan chan amqp.Confirmation
}

// PublishTransientMessage is similar to `Publish` but the message is transient
func (mq *WritableQueue) PublishTransientMessage(exchangeName, routingKey string, payload []byte) error {
	return mq.Publish(exchangeName, routingKey, amqp.Transient, payload)
}

// PublishPersistentMessage is similar to `Publish` but the message is persistent
func (mq *WritableQueue) PublishPersistentMessage(exchangeName, routingKey string, payload []byte) error {
	return mq.Publish(exchangeName, routingKey, amqp.Persistent, payload)
}

// Publish sends a new message to the `exchangeName` with `routingKey`
func (mq *WritableQueue) Publish(exchangeName, routingKey string, deliveryMode uint8, payload []byte) error {
	err := mq.Channel.Publish(
		exchangeName, // exchange
		routingKey,   // routing key
		false,        // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: deliveryMode,
			Body:         payload,
		})

	if err != nil {
		return err
	}

	if !mq.ConfirmMode {
		return nil
	}

	confirmed := <-mq.ConfirmsChan
	if !confirmed.Ack {
		return fmt.Errorf("failed to publish to [%s][%s]", exchangeName, routingKey)
	}

	return nil
}
