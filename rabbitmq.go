package gorabbitmq

import (
	"fmt"
	"strconv"

	"github.com/streadway/amqp"
	"github.com/tanqhnguyen/envconfig"
)

// RabbitMQConfig is the connection options for a rabbitmq client
type RabbitMQConfig struct {
	Host     string `envconfig:"host" default:"127.0.0.1"`
	Port     int    `envconfig:"port" default:"5672"`
	Username string `envconfig:"username" required:"true"`
	Password string `envconfig:"password" required:"true" file_content:"true"`
}

// GenerateRabbitMQConnectionString constructs the connection string based on the config struct
func GenerateRabbitMQConnectionString(c *RabbitMQConfig) string {
	return fmt.Sprintf("amqp://%s:%s@%s:%s/",
		c.Username,
		c.Password,
		c.Host,
		strconv.Itoa(c.Port),
	)
}

// LoadRabbitMQConfigFromEnvironment return the rabbitmq config as set in the environment variable
func LoadRabbitMQConfigFromEnvironment() (*RabbitMQConfig, error) {
	var fromEnv RabbitMQConfig
	err := envconfig.Process("RABBITMQ", &fromEnv)

	if err != nil {
		return nil, err
	}

	return &fromEnv, nil
}

// NewRabbitMQClientFromEnvironment establishes a channel to connect to RabbitMQ server
// using env variables
func NewRabbitMQClientFromEnvironment() *amqp.Connection {
	configFromEnv, err := LoadRabbitMQConfigFromEnvironment()
	if err != nil {
		panic("failed to load config from env")
	}

	connection, err := amqp.Dial(GenerateRabbitMQConnectionString(configFromEnv))
	if err != nil {
		password := "Empty or too short"
		if len(configFromEnv.Password) > 5 {
			password = configFromEnv.Password[0:5] + "***"
		}
		panic(fmt.Sprintf("failed to establish connection to server [%s:%d] for user [%s] with password [%s]", configFromEnv.Host, configFromEnv.Port, configFromEnv.Username, password))
	}

	return connection
}

// ConnectRabbitMQUsingEnvironment establishes a channel to connect to RabbitMQ server
// using env variables
func ConnectRabbitMQUsingEnvironment() *amqp.Connection {
	fmt.Println("ConnectRabbitMQUsingEnvironment is deprecated, use NewRabbitMQClientFromEnvironment instead")
	return NewRabbitMQClientFromEnvironment()
}
