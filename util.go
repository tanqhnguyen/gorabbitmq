package gorabbitmq

import (
	"fmt"

	"github.com/google/uuid"
)

// GenerateNewConsumerID generates a unique UUID
func GenerateNewConsumerID(prefix string) string {
	consumer, err := uuid.NewRandom()
	if err != nil {
		panic(err)
	}

	return fmt.Sprintf("%s:%s", prefix, consumer.String())
}
